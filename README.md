# Prototype OS project "identify and shoot" 2020: Mr Robot

<!-- Image references-->
[scheme]:scheme.png

## About

The identify and shoot project consists of building a robot using the EV3 Brick, to play a game where the robot has to detect objects, avoid obstacles and shoot balls to the opponents' side of the field. The programming was done thanks to techniques studied during the OS course.

For more info, check [report.md](./report.md)

## Setup
Inside the Makefile, make sure to update the following variables:
- `IP_ROBOT`: put the static IP of th robot on your local network
- `ROOT_DIR`: put the root directory of the project that contains all other files and directories
- `TARGETS`: add to the list the new source files you added to the project. This only helps with the cross-all and push-all commands. Other targets (e.g: cross-newfile, push-newfile) will work just fine with no modification.

Inside `./source/client.c` modify:
 - `SERV_ADDR`: Address of the game server
 - `SERV_ADDR`: Port of the game server
 -  `TEAM_ID`: Your team ID


## Folder structure




```
.
├── bin                    # Binary files
│   └── *
├── build
├── ev3dev-c
│   └── *                  # EV3dev depencencies
├── include                # Header files
│   ├── client.h
│   ├── data.h
│   └── motion.h
├── launch
├── libs
│   └── libev3dev-c.a
├── Makefile
├── README.md
├── report.md             # our report
├── *
├── scripts               # Scripts
│   ├── clear_ipcs          # Clears IPC memory written by sensors.c
│   └── stop_motion         # Stops all motors
└── source                # Source code
    ├── baseline.c
    ├── client.c
    ├── main.c
    ├── motion.c
    ├── sensors.c
    ├── test1.c
    ├── test1b.c
    ├── test4.c
    └── test5.c
```
