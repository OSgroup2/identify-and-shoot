#include "motion.h"
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_sensor.h"
#include "ev3_tacho.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define port_left OUTPUT_A
#define port_right OUTPUT_B
#define port_servo OUTPUT_D
FLAGS_T state;
uint8_t m_left;
uint8_t m_right;
uint8_t servo;
int max_speed_left;
int max_speed_right;
int max_servo;
int max_speed;

//initialize motors and servo
void pre() {
    while (ev3_tacho_init() < 1)
        sleep(1000);
    ev3_search_tacho_plugged_in(port_left, 0, &m_left, 0);
    ev3_search_tacho_plugged_in(port_right, 0, &m_right, 0);
    ev3_search_tacho_plugged_in(port_servo, 0, &servo, 0);
    get_tacho_max_speed(m_left, &max_speed_left);
    get_tacho_max_speed(m_right, &max_speed_right);
    get_tacho_max_speed(servo, &max_servo);
    max_speed = max_speed_right > max_speed_left ? max_speed_left : max_speed_right;
}

//stop motors
void done() {
    set_tacho_stop_action_inx(m_left, TACHO_COAST);
    set_tacho_stop_action_inx(m_right, TACHO_COAST);
    set_tacho_command_inx(m_left, TACHO_STOP);
    set_tacho_command_inx(m_right, TACHO_STOP);
}

//move forward
int forward(int speed) {
    if (speed == 0 || speed > max_speed)
        speed = max_speed;
    set_tacho_speed_sp(m_left, speed);
    set_tacho_speed_sp(m_right, speed);
    set_tacho_command_inx(m_left, TACHO_RUN_FOREVER);
    set_tacho_command_inx(m_right, TACHO_RUN_FOREVER);
    return 0;
}

//move only with the left servo
int left_only(int speed) {
    if (speed == 0 || speed > max_speed)
        speed = max_speed;
    set_tacho_speed_sp(m_left, speed);
    set_tacho_stop_action_inx(m_right, TACHO_COAST);
    set_tacho_command_inx(m_right, TACHO_STOP);
    set_tacho_command_inx(m_left, TACHO_RUN_FOREVER);
    return 0;
}

//move only with the right servo
int right_only(int speed) {
    if (speed == 0 || speed > max_speed)
        speed = max_speed;
    set_tacho_speed_sp(m_left, speed);
    set_tacho_stop_action_inx(m_left, TACHO_COAST);
    set_tacho_command_inx(m_left, TACHO_STOP);
    set_tacho_command_inx(m_right, TACHO_RUN_FOREVER);
    return 0;
}

//move backward
int backward(int speed) {
    if (speed == 0 || speed > max_speed)
        speed = max_speed;
    speed = -speed;
    set_tacho_speed_sp(m_left, speed);
    set_tacho_speed_sp(m_right, speed);
    set_tacho_command_inx(m_left, TACHO_RUN_FOREVER);
    set_tacho_command_inx(m_right, TACHO_RUN_FOREVER);
    return 0;
}

//stop with hard brake
int stop() {
    set_tacho_stop_action_inx(m_left, TACHO_HOLD);
    set_tacho_stop_action_inx(m_right, TACHO_HOLD);
    set_tacho_command_inx(m_left, TACHO_STOP);
    set_tacho_command_inx(m_right, TACHO_STOP);
    return 0;
}

//stop without hard brake
int stop_soft() {
    set_tacho_stop_action_inx(m_left, TACHO_BRAKE);
    set_tacho_stop_action_inx(m_right, TACHO_BRAKE);
    set_tacho_command_inx(m_left, TACHO_STOP);
    set_tacho_command_inx(m_right, TACHO_STOP);
    return 0;
}

//move servo (controlling sonar angle)
int servo_move(int position, int speed) {
    if (speed == 0)
        speed = max_servo;
    set_tacho_speed_sp(servo, speed);
    set_tacho_position_sp(servo, position);
    set_tacho_command_inx(servo, TACHO_RUN_TO_REL_POS);
    do {
        get_tacho_state_flags(servo, &state);
    } while (state);
    return 0;
}

// spin robot
int spin(int speed) {
    if (speed == 0 || speed > max_speed)
        speed = max_speed;
    speed = -speed;
    set_tacho_speed_sp(m_left, speed);
    set_tacho_speed_sp(m_right, -speed);
    set_tacho_command_inx(m_left, TACHO_RUN_FOREVER);
    set_tacho_command_inx(m_right, TACHO_RUN_FOREVER);
    return 0;
}
/*
int granular_servo_move(int position, int speed){
  int i;
  for (i=)
}*/
