// Grabs values from sensors and puts them into shared memory
// arguments:
//  1. verbose: "1", non-verbose: "0"

#include "data.h"
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_sensor.h"
#include "ev3_tacho.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#define port_left OUTPUT_A
#define port_right OUTPUT_B

struct DATA;

int main(int argc, char *argv[]) {
    FLAGS_T state_l, state_r;
    uint8_t sn_color;
    uint8_t sn_gyro;
    uint8_t sn_compass;
    uint8_t sn_sonar;
    uint8_t m_left;
    uint8_t m_right;
    int memid;
    int this_pid;
    struct DATA *data;
    (void)argc;
    this_pid = (int)getpid();

    if (argc < 2) {
        perror("Not enough arguments supplied");
        return (EXIT_FAILURE);
    }

    //Initialize shared memory
    key_t key = 6969;
    memid = shmget(key, sizeof(DATA), 0700 | IPC_CREAT);
    if (memid == -1) {
        perror("shmget");
        return (EXIT_FAILURE);
    }
    data = shmat(memid, NULL, 0);

    data->sensors_pid = this_pid; //Register current pid in the data struct

    //Run all sensors
    ev3_sensor_init();
    ev3_search_sensor(LEGO_EV3_COLOR, &sn_color, 0);
    ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro, 0);
    ev3_search_sensor(HT_NXT_COMPASS, &sn_compass, 0);
    ev3_search_sensor(LEGO_EV3_US, &sn_sonar, 0);

    (void)argc;
    while (ev3_tacho_init() < 1)
        sleep(1000);
    ev3_search_tacho_plugged_in(port_left, 0, &m_left, 0);
    ev3_search_tacho_plugged_in(port_right, 0, &m_right, 0);

    while (1) {
        usleep(100);
        get_sensor_value(0, sn_color, &data->color);
        get_sensor_value(0, sn_gyro, &data->gyro);
        get_sensor_value(0, sn_compass, &data->compass);
        get_sensor_value(0, sn_sonar, &data->sonar);
        get_tacho_state_flags(m_left, &state_l);
        get_tacho_state_flags(m_right, &state_r);
        data->state_left = (int)state_l;
        data->state_right = (int)state_r;
        if (atoi(argv[1]) == 1) {
            printf("\033[0G | color= %d |"
                   "gyro= %d |"
                   "compass = %d |"
                   "sonar= %d |"
                   "state_left=%d |"
                   "state_right=%d |",
                   data->color, data->gyro, data->compass, data->sonar,
                   data->state_left, data->state_right);
        }
        if (data->sensors_pid != this_pid)
            return (EXIT_FAILURE); // If a new instance is launched, we exit this one
    }
    return (EXIT_SUCCESS);
}
