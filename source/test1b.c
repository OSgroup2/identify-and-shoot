//Be able to find an object in your side (without identifying its shape). For this test, only one object is put in your side.
// Adiitional approach, turned out it didn't work as expected

#include "data.h"
#include "motion.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#define COLOR_THRESHOLD 45  //10
#define SONAR_THRESHOLD 200 //700
struct DATA;

int main(int argc, char *argv[]) {
    struct DATA *data;
    int memid;

    // This a shared section between all programs
    // It gives the program access to the shared memory
    // A simple key is used instead of ftoc() that generates a key from a file
    // because the os on ev3 is only running few processes, none of them
    // using system v with this key
    key_t key = 6969;
    //initialize memory
    memid = shmget(key, sizeof(DATA), 0700 | IPC_CREAT);
    if (memid == -1) {
        perror("shmget");
        return (EXIT_FAILURE);
    }
    //mounting the shared memory region
    data = shmat(memid, NULL, 0);

    //init the tachos motors
    pre();

    printf("sonar: %d\nColor:%d\n", data->sonar, data->color);

    //move
    left_only(200);
    while (data->sonar > SONAR_THRESHOLD) {
        usleep(5);
        printf("sonar: %d\n Color:%d\n", data->sonar, data->color);
    }
    forward(250);
    sleep(1);
    done();

    return 0;
}
