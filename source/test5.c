// Be able to shoot a ball in the opposite side. You can place your robot and the ball as you wish.
// arguments:
//  1. Time to move forward
//  2. Time to move backward
// Hit the ball run with ./test5 1400000 4600000

#include "data.h"
#include "motion.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>
#define COLOR_THRESHOLD 45  //10
#define SONAR_THRESHOLD 250 //700
#define BALL_ANGLE 65
struct DATA;
int main(int argc, char *argv[]) {

    struct DATA *data;
    int memid, sleep_time_1, sleep_time_2;
    // This a shared section between all programs
    // It gives the program access to the shared memory
    // A simple key is used instead of ftoc() that generates a key from a file
    // because the os on ev3 is only running few processes, none of them
    // using system v with this key
    key_t key = 6969;
    //initialize memory
    memid = shmget(key, sizeof(DATA), 0700 | IPC_CREAT);
    if (memid == -1) {
        perror("shmget");
        return (EXIT_FAILURE);
    }
    //mounting the shared memory region
    data = shmat(memid, NULL, 0);

    // load args
    if (argc < 3) {
        perror("Not enough arguments");
        exit(-1);
    }
    sleep_time_1 = atoi(argv[1]);
    sleep_time_2 = atoi(argv[2]);

    //init the tachos motors
    pre();

    //some logging

    printf("sonar: %d\nColor:%d\n", data->sonar, data->color);
    //move
    int gyro_zero = data->gyro;
    //spin till reaching the ball angle

    while (abs(data->gyro - gyro_zero) < BALL_ANGLE) {
        spin(40);
    }
    stop_soft();

    //run towards the ball with mac speed = 0
    forward(0);
    //keep running for argv1 useconds
    usleep(sleep_time_1);
    stop_soft();
    sleep(1);
    //go back to initial position
    backward(200);
    usleep(sleep_time_2);
    //more logging
    printf("data gyro: %d\ngyro_zero:%d\n", data->gyro, gyro_zero);

    //spin back to inital angle
    while (data->gyro < gyro_zero) {
        spin(-40);
    }
    stop_soft();
    backward(20);
    sleep(2);
    stop_soft();

    stop_soft();

    done();

    return 0;
}
