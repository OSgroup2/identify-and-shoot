// Be able to find an  Be able to find a ball located at a predefined position.
// arguments:
//  1. Detection threshold value
//  2. baseline filename
#include "data.h"
#include "motion.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
struct DATA;
struct DATA *data;
int main(int argc, char *argv[]) {
    int memid;
    int gyro_zero;
    int spin_time;
    int old_slice;
    int sum;
    int slice;
    int counter;
    int slices[400];
    int spin_speed;

    // This a shared section between all programs
    // It gives the program access to the shared memory
    // A simple key is used instead of ftoc() that generates a key from a file
    // because the os on ev3 is only running few processes, none of them
    // using system v with this key
    key_t key = 6969;
    //initialize memory
    memid = shmget(key, sizeof(DATA), 0700 | IPC_CREAT);
    if (memid == -1) {
        perror("shmget");
        return (EXIT_FAILURE);
    }
    //mounting the shared memory region
    data = shmat(memid, NULL, 0);

    //Load args
    if (argc < 2) {
        perror("Not enough arguments");
        exit(-1);
    }
    spin_speed = atoi(argv[1]);

    //init the tachos motors
    pre();
    gyro_zero = data->gyro;
    spin(spin_speed);

    // Move to the angle of the ball
    while ((int)(gyro_zero - data->gyro < 370)) {
        slice = (int)((gyro_zero - data->gyro) / 5) * 5;
        if (slice == 300) {
            break;
        }
    }

    // Stop spinning and drive towards the ball for 1.1 second
    stop();
    forward(0);
    sleep(1.1);
    printf("found\n");
    stop();
    return 0;
}
