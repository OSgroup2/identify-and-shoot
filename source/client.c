// Library functions for the networking client.
// Author: Ruben
// Important:
// 1. Errors handled internally, and makes program exit.
// 2. server_initialize must be called at the start of the program.
// 3. server_close must be called at the end of the program.

#include "client.h"
#include <arpa/inet.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

#define SERV_ADDR "10.42.0.1"
#define SERV_PORT 1024
#define TEAM_ID 2 /* Your team ID */

#define MSG_ACK 0
#define MSG_START 1
#define MSG_STOP 2
#define MSG_KICK 3
#define MSG_OBJ_ID 5
#define MSG_TEST 4
#define SA struct sockaddr
#define Sleep(msec) usleep((msec)*1000)

int s;
uint16_t msgId = 0;
uint8_t *buffer;
int iteration;

int sockfd, connfd;

// This reads from 0 to maxSize bits from server and it stores it in buffer
int read_from_server(int sock, uint8_t *buffer, size_t maxSize) {

    int bytes_read = read(sock, buffer, maxSize);

    if (bytes_read <= 0) {
        fprintf(stderr, "[ERR] Server unexpectedly closed connection...\n");
        close(s);
        exit(EXIT_FAILURE);
    }

    printf("[DEBUG] Received %d bytes\n", bytes_read);
    return bytes_read;
}

// Sends test message to server, exists on error
void server_test() {
    /*s
    	Send a TEST request
    	Receive ACK	
    */
    printf("[DEBUG] Ready to send a TEST message\n");

    uint16_t idTestmsg = 29000; //id random
    buffer[0] = (uint8_t)idTestmsg;
    buffer[1] = (uint8_t)(idTestmsg >> 8);
    buffer[2] = TEAM_ID;  //src, us
    buffer[3] = 255;      //dst, server (id=255)
    buffer[4] = MSG_TEST; //Message type
    buffer[5] = 0;
    buffer[6] = 0;
    buffer[7] = 0; //3 random bytes
    write(sockfd, buffer, 8);

    printf("[DEBUG] Ready to receive an ACK\n");
    read_from_server(sockfd, buffer, 8);
    //uint16_t id = buffer[0] | buffer[1] << 8; //i don't care about ack id
    uint8_t src = buffer[2];
    uint8_t dst = buffer[3];
    uint8_t type = buffer[4];
    uint8_t error = buffer[5];
    uint16_t idConfirmed = buffer[6] | buffer[7] << 8;

    if (src != 255) {
        printf("[ERR] I expected a message from the server (id=255), but I received a message from a foreigner (id sender = %d)\n", src);
        free(buffer);
        exit(65);
    }

    if (dst != TEAM_ID) {
        printf("[ERR] I expected a message addressed to me (id = %d), but I wasn't the receiver (id receiver = %d)\n", TEAM_ID, dst);
        free(buffer);
        exit(75);
    }

    if (type != 0) {
        printf("[ERR] I expected an ACK (type=0), but I received another type of message\n");
        free(buffer);
        exit(55);
    }

    if (error > 0) {
        printf("[ERR] My last message was not well processed: %d\n", error);
        free(buffer);
        exit(85);
    }

    if (idConfirmed != idTestmsg) {
        printf("[ERR] ID issue\n");
        free(buffer);
        exit(95);
    }

    printf("[DEBUG] ACK received\n");
}

//Listens for STOP message from server, on error: exit
void server_receive_stop() {
    printf("[DEBUG] Ready to receive a STOP\n");

    read_from_server(sockfd, buffer, 8);
    uint8_t src = buffer[2];
    uint8_t dst = buffer[3];
    uint8_t type = buffer[4];
    uint8_t error = buffer[5];
    //uint16_t id = buffer[0] | buffer[1] << 8; //i don't care about ack id

    // the rest is random

    if (src != 255) {
        printf("[ERR] I expected a message from the server (id=255), but I received a message from a foreigner (id sender = %d)\n", src);
        free(buffer);
        exit(65);
    }

    if (dst != TEAM_ID) {
        printf("[ERR] I expected a message addressed to me (id = %d), but I wasn't the receiver (id receiver = %d)\n", TEAM_ID, dst);
        free(buffer);
        exit(75);
    }

    if (type != 2) {
        printf("[ERR] I expected a STOP (type=2), but I received another type of message\n");
        free(buffer);
        exit(55);
    }

    printf("[DEBUG] STOP received\n");
}
// Sends object location and type to the server, on failure: exit
void server_send_object(int typeObj, int x, int y) {
    /*

        1 for cube
        2 for a 4-side pyramid
        3 for a 3-side pyramid
        4 for a 4-side pyramid upside-down
        5 for a 3-side pyramid upside-down
        6 for cylinder
    */

    //Send  "cylinder identified" at position (6, 12)
    iteration++;
    printf("[DEBUG] Ready to send a OBJ_ID message (%d at position (%d, %d)\n", typeObj, x, y);
    uint16_t idCylindermsg = 37854; //id
    buffer[0] = (uint8_t)idCylindermsg;
    buffer[1] = (uint8_t)(idCylindermsg >> 8);
    buffer[2] = TEAM_ID;                //src, us
    buffer[3] = 255;                    //dst, server (id=255)
    buffer[4] = MSG_OBJ_ID + iteration; //Message type
    buffer[5] = typeObj;                //cylinder is 6
    buffer[6] = x;                      //x
    buffer[7] = y;                      //y
    write(sockfd, buffer, 8);

    printf("[DEBUG] Ready to receive an ACK\n");

    read_from_server(sockfd, buffer, 8);

    uint8_t src = buffer[2];
    uint8_t dst = buffer[3];
    uint8_t type = buffer[4];
    uint8_t error = buffer[5];
    uint16_t idConfirmed = buffer[6] | buffer[7] << 8;

    if (src != 255) {
        printf("[ERR] I expected a message from the server (id=255), but I received a message from a foreigner (id sender = %d)\n", src);
        free(buffer);
        exit(65);
    }

    if (dst != TEAM_ID) {
        printf("[ERR] I expected a message addressed to me (id = %d), but I wasn't the receiver (id receiver = %d)\n", TEAM_ID, dst);
        free(buffer);
        exit(75);
    }

    if (type != 0) {
        printf("[ERR] I expected an ACK (type=0), but I received another type of message\n");
        free(buffer);
        exit(55);
    }

    if (error > 0) {
        printf("[ERR] My last message was not well processed: %d\n", error);
        free(buffer);
        exit(85);
    }

    if (idConfirmed != idCylindermsg) {
        printf("[ERR] ID issue\n");
        free(buffer);
        exit(95);
    }

    printf("[DEBUG] ACK received\n");
}

//Send ball kicked message, on error: exit
void server_kick_ball(uint8_t dest, uint8_t x, uint8_t y) {
    printf("[DEBUG] Ready to send a ball kicked\n");
    uint16_t idKickmsg = 37854; //id
    buffer[0] = (uint8_t)idKickmsg;
    buffer[1] = (uint8_t)(idKickmsg >> 8);
    buffer[2] = TEAM_ID; //src, us
    buffer[3] = 255;     //dst, server (id=255)
    buffer[4] = 3;       //Message type
    buffer[5] = x;       //x
    buffer[6] = y;       //y
    buffer[7] = 39;      //random
    write(sockfd, buffer, 8);

    printf("[DEBUG] Ready to receive an ACK\n");

    read_from_server(sockfd, buffer, 8);

    uint8_t src = buffer[2];
    uint8_t dst = buffer[3];
    uint8_t type = buffer[4];
    uint8_t error = buffer[5];
    uint16_t idConfirmed = buffer[6] | buffer[7] << 8;
    //uint16_t id = buffer[0] | buffer[1] << 8; //i don't care about ack id
    src = buffer[2];
    dst = buffer[3];
    type = buffer[4];
    error = buffer[5];
    idConfirmed = buffer[6] | buffer[7] << 8;

    if (src != 255) {
        printf("[ERR] I expected a message from the server (id=255), but I received a message from a foreigner (id sender = %d)\n", src);
        free(buffer);
        exit(65);
    }

    if (dst != TEAM_ID) {
        printf("[ERR] I expected a message addressed to me (id = %d), but I wasn't the receiver (id receiver = %d)\n", TEAM_ID, dst);
        free(buffer);
        exit(75);
    }

    if (type != 0) {
        printf("[ERR] I expected an ACK (type=0), but I received another type of message\n");
        free(buffer);
        exit(55);
    }

    if (error > 0) {
        printf("[ERR] My last message was not well processed: %d\n", error);
        free(buffer);
        exit(85);
    }

    if (idConfirmed != idKickmsg) {
        printf("[ERR] ID issue\n");
        free(buffer);
        exit(95);
    }

    printf("[DEBUG] ACK received\n");
}

// initializes server and waits for start message, on error: exit
void server_initialize() {
    struct sockaddr_in servaddr, cli;

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        printf("[ERR] Socket creation failed...\n");
        exit(0);
    } else
        printf("[DEBUG] Socket successfully created..\n");
    bzero(&servaddr, sizeof(servaddr));

    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(SERV_ADDR);
    servaddr.sin_port = htons(SERV_PORT);

    // connect the client socket to server socket
    if (connect(sockfd, (SA *)&servaddr, sizeof(servaddr)) != 0) {
        printf("[ERR] Connection with the server failed...\n");
        exit(0);
    } else
        printf("[DEBUG] Connected to the server..\n");

    //Receive the start message and print the various parameters
    buffer = (uint8_t *)malloc(8 * sizeof(uint8_t));
    if (buffer == NULL) {
        printf("[ERR] Memory finished\n");
    }
    read_from_server(sockfd, buffer, 8);

    uint16_t id = buffer[0] | buffer[1] << 8;

    uint8_t src = buffer[2];
    uint8_t dst = buffer[3];
    uint8_t type = buffer[4];

    printf("[DEBUG] ID of the start: %d\n", id);
    printf("[DEBUG] From: %d\n", src);
    printf("[DEBUG] To: %d\n", dst);
    printf("[DEBUG] Type: %d\n", type);

    if (type == 1) {
        printf("[DEBUG] START message received\n");
    } else {
        printf("[ERR] I expected a START message, but another message arrived\n");
        exit(55);
    }

    if (src != 255) {
        printf("[ERR] I expected a message from the server (id=255), but I received a message from a foreigner\n");
        exit(65);
    }

    if (dst != TEAM_ID) {
        printf("[ERR] I expected a message addressed to me (id = %d), but I wasn't the receiver (id receiver = %d)\n", TEAM_ID, dst);
        exit(75);
    }
    return;
}

//Closes connection and cleans up environment
void server_close() {
    // close the socket
    close(sockfd);
    free(buffer);
}