// Be able to find an object in your side (without identifying its shape). For this test, only one object is put in your side.
// arguments:
//  1. Detection threshold value
//  2. baseline filename

#define _GNU_SOURCE
#include "data.h"
#include "motion.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
struct DATA;
struct DATA *data;
FILE *fp;
time_t t;
int main(int argc, char *argv[]) {
    int memid;
    int gyro_zero;
    int old_slice;
    int sum;
    int slice;
    int counter;
    int scan[73];
    int baseline[73];
    int diff[73];
    char *line = NULL;
    size_t len = 0;
    size_t read;
    int i;
    int max_cluster;
    int current_cluster;
    int f_limit;
    int s_limit;
    int target;
    double threshold;
    char *filename;

    // This a shared section between all programs
    // It gives the program access to the shared memory
    // A simple key is used instead of ftoc() that generates a key from a file
    // because the os on ev3 is only running few processes, none of them
    // using system v with this key
    key_t key = 6969;
    //initialize memory
    memid = shmget(key, sizeof(DATA), 0700 | IPC_CREAT);
    if (memid == -1) {
        perror("shmget");
        return (EXIT_FAILURE);
    }
    //mounting the shared memory region
    data = shmat(memid, NULL, 0);

    //init the tachos motors
    pre();

    if (argc < 3) {
        perror("Not enough arguments");
        exit(-1);
    }
    //load arguments
    threshold = atof(argv[1]);
    filename = argv[2];

    //saving the initial vaue of the gyro to track the spin
    gyro_zero = data->gyro;
    old_slice = 0;
    sum = 0;
    counter = 0;
    spin(20);
    t = time(NULL);

    //a loop that keeps logging the sonar output while spining
    while (abs(gyro_zero - data->gyro) < 370) {
        printf("%d\n", abs(gyro_zero - data->gyro));
        slice = (int)((gyro_zero - data->gyro) / 5);

        //detecting contiguous clusters
        if (slice != old_slice) {
            scan[old_slice] = (int)sum / counter;
            old_slice = slice;
            sum = 0;
            counter = 0;
        }
        sum = sum + data->sonar;
        counter++;
    }

    //stop the spin
    stop();

    fp = fopen("baseline.txt", "r");
    if (fp == NULL)
        printf("error reading the baseline");

    //compare scan against baseline
    i = 0;
    while ((read = getline(&line, &len, fp)) != -1) {
        baseline[i] = atoi(line);
        if (abs(scan[i] - baseline[i]) > threshold * baseline[i]) { //threshold between 0.0 and 1.0

            diff[i] = 1;
            //compare scan to baseline with a tolerance threshold
            printf("%d\n", scan[i] - baseline[i]);
        } else
            diff[i] = 0;
        i++;
    }
    fclose(fp);

    //free memory
    if (line)
        free(line);

    //compare and sort detected clusters

    max_cluster = 0;
    current_cluster = 0;
    for (i = 1; i < 73; i++) {
        printf("%d", diff[i]);

        //detect contiguous regions of the bitmap
        if ((diff[i] == 1) & (diff[i] == diff[i - 1])) {
            current_cluster++;
        } else {
            if (current_cluster > max_cluster) {
                max_cluster = current_cluster;
                f_limit = i - 1;
                s_limit = f_limit - max_cluster;
                target = (s_limit - 2 + (int)((f_limit - 1 - s_limit)) / 2) * 5;
                current_cluster = 0;
            }
        }
    }

    //spin to the right angle and then drive towards the object at the specified angle
    spin(100);
    gyro_zero = data->gyro;
    while (abs(data->gyro - gyro_zero) < target * 1.01) {
        usleep(10);
    }
    stop();
    forward(100);
    printf("\nmax_cluster = %d \nf_limit = %d\n target = %d \n", max_cluster, f_limit, target);

    //approach object
    while (data->sonar > 100) {
        usleep(10);
    }
    stop();

    return 0;
}
