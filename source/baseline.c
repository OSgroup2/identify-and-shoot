// Gets the baseline sonar values on every 5 degree angle from the intial starting position
// Author: Youssef, Mokhles
// Arguments:
//   1. output filename
//   2. spin speed

#include "data.h"
#include "motion.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
struct DATA;
struct DATA *data;
FILE *fp;
time_t t;
int main(int argc, char *argv[]) {
    int memid;
    int gyro_zero;
    int spin_time;
    char *filename;
    int old_slice;
    int sum;
    int slice;
    int counter;

    if (argc < 3) {
        perror("Not enough arguments supplied");
    }

    filename = argv[1];

    //Initalize shared memory
    key_t key = 6969;
    memid = shmget(key, sizeof(DATA), 0700 | IPC_CREAT);
    if (memid == -1) {
        perror("shmget");
        return (EXIT_FAILURE);
    }
    data = shmat(memid, NULL, 0);

    fp = fopen(filename, "a+");
    pre();
    gyro_zero = data->gyro;
    old_slice = 0;
    sum = 0;
    counter = 0;
    spin(atoi(argv[2]));
    t = time(NULL);
    while (gyro_zero - data->gyro < 370) {
        slice = (int)((gyro_zero - data->gyro) / 5) * 5;
        if (slice != old_slice) {
            fprintf(fp, "%d\n", sum / counter);
            printf("%d\n", sum / counter);
            old_slice = slice;
            sum = 0;
            counter = 0;
        }
        sum = sum + data->sonar;
        counter++;
    }
    stop();
    fclose(fp);
    return 0;
}
