void pre() ;
int forward(int speed);
int backward(int speed);
int left_only(int speed);
int right_only(int speed);
int stop();
int servo_move(int position, int speed);
void done();

