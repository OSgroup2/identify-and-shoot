typedef struct DATA{
  int color;
  int compass;
  int gyro;
  int sonar;
  int sensors_pid;
  int x;
  int y;
  int rotations_count;
  int state_left;
  int state_right;
  int angle;
} DATA;
