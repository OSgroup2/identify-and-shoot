#include <stdint.h>

void server_test ();
void server_receive_stop();
void server_send_object(int typeObj, int x , int y);
void server_kick_ball(uint8_t dest, uint8_t x, uint8_t y);
void server_initialize();
void server_close();