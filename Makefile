CC 		= arm-linux-gnueabi-gcc
CFLAGS 		= -O2 -g -std=gnu99 -W -Wall -Wno-comment
INCLUDES 	= -I./ev3dev-c/source/ev3 -I./include/
LDFLAGS 	= -L./libs -lrt -lm -lev3dev-c -lpthread
BUILD_DIR 	= ./build
SOURCE_DIR 	= ./source
BINARY_DIR = ./bin
IP_ROBOT 	= 10.42.0.123
ROOT_DIR = ~/os_robot
TARGETS = sensors baseline test1 test4 test5 main 

all: $(TARGETS)

%: $(BUILD_DIR)/%.o $(BUILD_DIR)/motion.o $(BUILD_DIR)/client.o
		$(CC) $(INCLUDES) $(CFLAGS) $(BUILD_DIR)/motion.o $(BUILD_DIR)/client.o $(BUILD_DIR)/$*.o $(LDFLAGS) -o $(BINARY_DIR)/$*

push-%:
	scp $(BINARY_DIR)/$* robot@$(IP_ROBOT):~/bin/

push-all:
	scp $(TARGETS:%=$(BINARY_DIR)/%) robot@$(IP_ROBOT):~/bin/

cross-%:
		docker run --rm -it -v $(ROOT_DIR):/src -w /src ev3dev/debian-jessie-cross make clean $*

$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.c
	$(CC) -c $(SOURCE_DIR)/$*.c $(INCLUDES) -o $(BUILD_DIR)/$*.o

clean:
	rm -f $(BUILD_DIR)/*.o
